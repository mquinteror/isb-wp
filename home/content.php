<div class="row">
    <div class="col s12 m10 offset-m1 l10 offset-l1 center-align">
	<h4>El Internado Sección "B" abre sus puertas</h4>
	<span class="left-align">
	    El I.S."B" recibe a jóvenes originarios del interior de la reública que se encuentren realizando sus estudios de nivel medio superior,
	    profesional y posgrado en la Ciudad de México. Los cuales deben de cumplir una serie de requisitos básicos. Así como realizar las
	    actividades cívicas, deportivas e deológicas propias del Pentathlón Deportivo Militarizado Universitario.
	</span>
    </div>
    
</div>
