<div class="row">
    <div class="col s12 m10 offset-m1 l10 offset-l1">
	<h5>
	    Estamos ubicados en el número 204 de la calle de José Antonio Alzate, Colonia Santa María la Rivera, en la delegación Cuauhtémoc, Ciudad de México.
	</h5>
	<h6>
	    A unas cuadras del metro Normal y el metro Sán Cósme de la línea 2 del metro.
	</h6>
    </div>
    <div class="col s12 m10 offset-m1 l 10 offset-l1">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4474.0785907218715!2d-99.16480250431478!3d19.445361184289087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f8c6932a97f3%3A0xffe58f9be031e557!2sInternado+Secci%C3%B3n+%22B%22+Gustavo+Baz+Prada!5e0!3m2!1sen!2smx!4v1465898354134" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col s12 m10 offset-m1 l10 offset-l1">
	<h5>
	    Entrada por Eje 1 norte José Antonio Alzate
	</h5>
    </div>
    <div class="col s12 m10 offset-m1 l10 offset-l1">
	<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2smx!4v1465898231095!6m8!1m7!1sQ8vGNZ6zGwCL6vyUD--IgQ!2m2!1d19.44806663476342!2d-99.16366116629449!3f195.33134281158553!4f-3.918380755604872!5f0.7820865974627469" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
