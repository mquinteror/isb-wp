$(document).ready(function(){
    $('.collapsible').collapsible();
});
// modal
$(document).ready(function(){
    $('.modal').modal();
});
// sidenav
$(document).ready(function(){
    $('.sidenav').sidenav();
});
// inicializacion de tabs
$(document).ready(function(){
    $('.tabs').tabs();
});
// inicializacion de carousel
$(document).ready(function(){
    $('.carousel').carousel();
});
// carousell-slider
$('.carousel.carousel-slider').carousel({
    fullWidth: true
});
// carousel con contenido
$('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true
});
//parallax
$(document).ready(function(){
    $('.parallax').parallax();
});

// script de busqueda de google
(function() {
    var cx = '007838508948113531460:fuqzn-7_olq';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
})();
