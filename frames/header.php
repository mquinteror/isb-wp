<head>
    <link rel="shortcut icon" type="image/x-icon" href="../multimedia/icons/logodorado.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/sticky-nav.css" />
    <link type="text/css" rel="stylesheet" href="../css/footer.css" />
    <link type="text/css" rel="stylesheet" href="../css/social-netwoks.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="../css/materialize-social.css" />	

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8" >
    <title>Internado Sección "B" Doctor Gustavo Baz Prada</title>	
</head>
