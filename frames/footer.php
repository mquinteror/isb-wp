<footer class="page-footer blue-grey ">
    <div class="container">
	<div class="row">
	    <div class="col l6 s12">
		<h5 class="white-text">Internado Sección "B" Doctor Gustavo Baz Prada</h5>
		<h6 class="grey-text text-lighten-4">
		    Organización sin fines de lucro fundada en 1941.
		    <p> Residencia estudiantil para jovenes originarios del interior de la republica
			que realizan estudios de nivel medio superior, licenciatura y posgrado en la
			Ciudad de México.
		    </p>	
		</h6>
	    </div>
	    <div class="col l4 offset-l2 s12">
		<ul>
		    <a class="waves-effect waves-light btn social facebook" href="https://www.facebook.com/ISBPDMU">
			<i class="fa fa-facebook"></i> ISB en Facebook  .
		    </a>
		    <a class="waves-effect waves-light btn social pinterest" href="https://www.youtube.com/channel/UClNIQ4rXXbGq7UQkcvA1Eag">
			<i class="fa fa-youtube"></i> El ISB en Youtube
		    </a>
		    <a class="waves-effect waves-light btn social instagram" href="https://www.instagram.com/internado_seccion_b">
			<i class="fa fa-instagram"></i> ISB en Instagram
		    </a>
		    <a class="waves-effect waves-light btn social google" href="https://plus.google.com/108469772705400823626">
			<i class="fa fa-google"></i> ISB en Google +
		    </a>		    
		</ul>
	    </div>
	</div>
    </div>
    <div class="footer-copyright">
	<div class="container">
	    José Antonio Alzate 204 Colonia Santa María la Rivera 06400 Delegación Cuauhtémoc, Ciudad de México
	    <a class="grey-text text-lighten-4 right" href="#!">Teléfono: 01 ( 55 ) 41 28 66</a>
	</div>
    </div>
</footer>

