<div class="row">
    <div class="col s2 m1 offset-m11 l1 offset-l11 social-networks right-align">
	<a class="waves-effect waves-light btn-floating btn-large social facebook" href="https://www.facebook.com/ISBPDMU/">
	    <i class="fa fa-facebook"></i> Internado Seccón "B" en Facebook
	</a>
	<a class="waves-effect waves-light btn-floating btn-large social instagram" href="https://www.instagram.com/internado_seccion_b/">
	    <i class="fa fa-instagram"></i> Internado Sección "B" en Instagram
	</a>
	<a class="waves-effect waves-light btn-floating btn-large social pinterest" href="https://www.youtube.com/channel/UClNIQ4rXXbGq7UQkcvA1Eag">
	    <i class="fa fa-youtube"></i> Internado Sección "B" en Youtube
	</a>
	<a class="waves-effect waves-light btn-floating btn-large social google-plus" href="https://plus.google.com/108469772705400823626">
	    <i class="fa fa-google-plus"></i> Internado Sección "B" en Google plus
	</a>
    </div>
</div>
