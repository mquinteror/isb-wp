
<div class="row hide-on-med-and-down" style=" position:absolute; top:40px; z-index:3;">
    <div class="col s3 l4 m4">
	<img class="responsive-img" src="../multimedia/icons/logodorado.png">
    </div>
</div>

<div class="row hide-on-med-and-down blue-grey darken-2" style="background-image: url('../multimedia/imgs/jardin-1.png'); background-repeat: no-repeat; margin-bottom:0px;">
    <div class="col s1 m1 l1 center-align ">
    </div>
    <div class="col s1 m1 l1 center-align">
    </div>
    <div class="col s1 m1 l1 center-align">
    </div>
    <div class="col s1 m1 l1 center-align">
    </div>
    <div class="col s1 m1 l1 blue-grey lighten-3">
    </div>
    <div class="col s1 m1 l1 blue-grey lighten-4">
    </div>
    <div class="col s1 m1 l1 ">
    </div>
    <div class="col s1 m1 l1 ">
    </div>
    
    <!-- Barra de busqueda -->
    <div class=" col s3 m3 l3">
	<form name="busquedaGoogle" action="http://centos-lite.seccionb.mx/isb-wp/busqueda/index.php" method="get" >
	    <input type="text" placeholder="Buscar en sl sitio" id="isbgcs" name="q" />
	</form>
    </div>
    <div class="col s1 m1 l1">
	<form name="boton-busqueda">
	    <br>
	    <input type="submit" value="Buscar" style="background: white" />
	</form>
    </div>
    <div class="col s12  m12 l12">
	<h5 class="center-align amber-text lighten-1" style:"font-family: Gill Sans / Gill Sans MT, sans-serif;">PENTATHLÓN DEPORTIVO MILITARIZADO UNIVERSITARIO</h5>
    </div>
    <div class="col s12 m12 l12 right-align ">
	<h3 class="center-align white-text lighten-3">INTERNADO SECCIÓN "B"</h3>
    </div>
    <div class="col s12 m12 l12 left-align ">
	<h5 class="center-align amber-text lighten-1">DOCTOR GUSTAVO BAZ PRADA</h5>
    </div>
</div>

<nav>
    <div class="nav-wrapper blue-grey row">
	<div class="col s1 show-on-small">
	    <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
	</div>
	<div class="col m12 l12 right-align hide-on-med-and-down">
	    <ul id="nav-mobile">
		<li><a href="../home/">Inicio</a></li>
		<li><a href="../contacto/">Contacto</a></li>
		<li><a href="../conocenos/">Conocenos</a></li>
		<li><a href="../servicios/">Servicios</a></li>
		<li><a href="../historia/">Historia</a></li>
		<li><a href="../ubicacion/">Ubicacion</a></li>
		<li><a href="../admision/">Admision</a></li>
		<li><a href="../vida-institucional/">Vida Institucional</a></li>
		<li><a href="../estudiantes/">Estudiantes</a></li>
		<li><a href="../internos/">Internos</a></li>
		<li><a href="../egresados/">Egresados</a></li>
	    </ul>
	</div>
	<div class="row hide-on-large-only">
	    <div class=" col s9">
		<form name="busquedaGoogle" action="http://centos-lite.seccionb.mx/isb-wp/busqueda/index.php" method="get" >
		    <input type="text" placeholder="Buscar en sl sitio" id="isbgcs" name="q" />
		</form>
	    </div>
	    <div class="col s1 m1 l1">
		<form name="boton-busqueda">
		    <input type="submit" value="Buscar" style="background: white" />
		</form>
	    </div>
	</div>
    </div>
</nav>
