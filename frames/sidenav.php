<ul id="slide-out" class="sidenav">
    <li><img src="../multimedia/imgs/fachada-balcon.png" class=" responsive-img"></li>
    <li><a href="../home/" class="waves-effect waves-teal lighten-2">Inicio</a></li>
    <li><a href="../contacto/" class="waves-effect waves-teal lighten-2">Contácto</a></li>
    <li><a href="../conocenos/" class="waves-effect waves-teal lighten-2">Conocenos</a></li>
    <li><a href="../servicios/" class="waves-effect waves-teal lighten-2">Servicios</a></li>
    <li><a href="../historia/" class="waves-effect waves-teal lighten-2">Historia</a></li>
    <li><a href="../ubicacion/" class="waves-effect waves-teal lighten-2">Ubicación</a></li>
    <li><a href="../admision/" class="waves-effect waves-teal lighten-2">Admisión</a></li>
    <li><a href="../vida-institucional/" class="waves-effect waves-teal lighten-2">Vida Institucional</a></li>
    <li><a href="../estudiantes/" class="waves-effect waves-teal lighten-2">Estudiantes</a></li>
    <ul class="collapsible">
	<li>
	    <div class="collapsible-header">
		<a href="../internos/" class="waves-effect waves-teal lighten-2">Internos</a>
	    </div>
	    <div class="collapsible-body">
		<ul>
		    <li>
			<div>
			    <a href="../internos/">Grupos</a>
			</div>
			<div>
			    <a href="../internos/">Documentos</a>
			</div>
			<div>
			    <a href="../internos/">Diviciones Profesionales</a>
			</div>
			<div>
			    <a href="../internos/">Noticias</a>
			</div>
		    </li>
		</ul>
	    </div>
	</li>
    </ul>
    <li><a href="../egresados/" class="waves-effect waves-teal lighten-2">Egresados</a></li>
</ul>
